﻿using System;
using CoreService.DataAccess.Models;
using Microsoft.EntityFrameworkCore;

namespace CoreService.DataAccess
{
    public class CoreContext: DbContext
    {
        public virtual DbSet<TripSearchParams> Trips { get; set; }

        public CoreContext(DbContextOptions<CoreContext> options)
        :base(options)
        {
            
        }

    }
}
