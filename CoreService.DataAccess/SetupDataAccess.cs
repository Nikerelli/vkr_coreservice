﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using CoreService.DataAccess.Mapping;
using Microsoft.Extensions.DependencyInjection;

namespace CoreService.DataAccess
{
    public static class SetupDataAccess
    {
        public static IServiceCollection SetUpDataAccess(this IServiceCollection services)
        {
            return services.AddSingleton<Profile, MainProfile>();
        }
    }
}
