﻿using System.Collections.Generic;

namespace CoreService.DataAccess.Models
{
    public class TripSearchParams
    {
        /// <summary/>
        public int Id { get; set; }

        /// <summary/>
        public string CompanyName { get; set; }

        /// <summary>
        ///     Trip route
        /// </summary>
        public string Route { get; set; }

        /// <summary>
        ///     Adults count
        /// </summary>
        public int Adults { get; set; }

        /// <summary>
        ///     Children count
        /// </summary>
        public int Children { get; set; }

        /// <summary/>
        public string FlightClass { get; set; }
    }
}
