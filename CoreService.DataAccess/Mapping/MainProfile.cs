﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CoreService.Business.Models.Request;
using CoreService.DataAccess.Models;

namespace CoreService.DataAccess.Mapping
{
    public class MainProfile : Profile
    {
        public MainProfile()
        {
            CreateMap<RequestFilter, TripSearchParams>()
                .ForMember(s => s.FlightClass, o => o.MapFrom(x => x.FlightClass.ToString()))
                .ForMember(s => s.Route,
                    o => o.MapFrom(x => string.Join(":", x.Route.Select(r => $"[{r.ArrivalIata}:{r.DepartureIata}]"))));
        }
    }
}
