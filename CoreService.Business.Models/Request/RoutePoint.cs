﻿using System;

namespace CoreService.Business.Models.Request
{
    /// <summary/>
    public class RoutePoint
    {
        /// <summary>
        ///     Departure location iata code
        /// </summary>
        public string DepartureIata { get; set; }

        /// <summary>
        ///     Arrival location iata code
        /// </summary>
        public string ArrivalIata { get; set; }

        /// <summary>
        ///     Departure time from location
        /// </summary>
        public DateTime? DepartureDate { get; set; }

        public string FlightDuration { get; set; }
    }
}
