﻿using System.Collections.Generic;
using CoreService.Business.Models.Enums;

namespace CoreService.Business.Models.Request
{
    /// <summary/>
    public class RequestFilter
    {
        /// <summary/>
        public string CompanyName { get; set; }

        /// <summary>
        ///     Trip route
        /// </summary>
        public IList<RoutePoint> Route { get; set; }

        /// <summary>
        ///     Adults count
        /// </summary>
        public int Adults { get; set; }

        /// <summary>
        ///     Children count
        /// </summary>
        public int Children { get; set; }

        /// <summary/>
        public FlightClass? FlightClass { get; set; }
    }
}
