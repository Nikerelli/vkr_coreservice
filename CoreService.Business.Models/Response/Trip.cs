﻿using System;
using System.Collections.Generic;
using CoreService.Business.Models.Enums;
using CoreService.Business.Models.Request;

namespace CoreService.Business.Models.Response
{
    public class Trip
    {
        public string CompanyName { get; set; }
        public IList<RoutePoint> Route { get; set; }

        public FlightClass? FlightClass { get; set; }

        public bool MeelIncluded { get; set; }

        public string TotalFlightpDuration { get; set; }
        public string TotalTripDuration { get; set; }

        public string DepartureCityIata { get; set; }
        public string ArrivalCityIata { get; set; }

        public bool FromCache { get; set; }
    }
}
