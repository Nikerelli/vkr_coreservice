﻿namespace CoreService.Business.Models.Enums
{
    public enum FlightClass
    {
        Economy,
        Business,
        FirstClass,
    }
}
