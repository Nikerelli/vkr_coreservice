﻿namespace CoreService.Contract.Enums
{
    public enum FlightClass
    {
        Economy,
        Business,
        FirstClass,
    }
}
