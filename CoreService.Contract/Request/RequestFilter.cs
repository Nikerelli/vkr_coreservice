﻿using CoreService.Contract.Enums;
using CoreService.Contract.Request;
using System.Collections.Generic;

namespace CoreService.Contract
{
    /// <summary/>
    public class RequestFilter
    {
        /// <summary>
        ///     Trip route
        /// </summary>
        public IList<RoutePoint> Route { get; set; }

        /// <summary>
        ///     Adults count
        /// </summary>
        public int Adults { get; set; }

        /// <summary>
        ///     Children count
        /// </summary>
        public int Children { get; set; }

        /// <summary/>
        public FlightClass? FlightClass { get; set; }
    }
}
