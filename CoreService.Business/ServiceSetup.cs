﻿using Microsoft.Extensions.DependencyInjection;

namespace CoreService.Business
{
    public static class ServiceSetup
    {
        public static IServiceCollection RegisterSearchService(this IServiceCollection services)
        {
            return services.AddSingleton<ISearchService, SearchService>();
        }
    }
}
