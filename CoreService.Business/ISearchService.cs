﻿using CoreService.Business.Models.Request;
using CoreService.Business.Models.Response;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoreService.Business
{
    public interface ISearchService
    {
        Task<IList<IList<Trip>>> SearchTripsAsync(RequestFilter filter);
    }
}
