﻿using System.Collections.Generic;

namespace CoreService.Business
{
    public interface ISearchConfig
    {
        List<string> Urls { get; set; }
    }
}
