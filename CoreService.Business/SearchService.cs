﻿using System;
using CoreService.Business.Models.Request;
using CoreService.Business.Models.Response;
using Flurl.Http;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using CoreService.DataAccess;
using Flurl.Http.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using AutoMapper;
using CoreService.DataAccess.Models;

namespace CoreService.Business
{
    internal class SearchService : ISearchService
    {
        private const string ApiKey = "";
        private readonly ISearchConfig _config;
        private readonly IMemoryCache _cacheStorage;
        private readonly ILogger<SearchService> _logger;
        private readonly IMapper _mapper;
        private readonly DbContextOptionsBuilder<CoreContext> _contextOptions;


        public SearchService(ISearchConfig config, IMemoryCache cacheStorage, ILogger<SearchService> logger,
            DbContextOptionsBuilder<CoreContext> ctxOptions, IMapper mapper)
        {
            _config = config;
            _cacheStorage = cacheStorage;
            _logger = logger;
            _contextOptions = ctxOptions;
            _mapper = mapper;
        }

        public async Task<IList<IList<Trip>>> SearchTripsAsync(RequestFilter filter)
        {
            // Проверка наличия ответа на запрос в кэше
            if (_cacheStorage.TryGetValue(JsonConvert.SerializeObject(filter), out object cacheResult))
            {
                var cachedResult = JsonConvert.DeserializeObject<IList<IList<Trip>>>(cacheResult as string);
                cachedResult.ToList().ForEach(x => x.ToList().ForEach(y => y.FromCache = true));
                return cachedResult;
            }

            await FixupReqeust(filter);

            // Поиск результатов от партнерских сервисов
            IList<IList<Trip>> searchResult = new List<IList<Trip>>();
            HttpClientHandler clientHandler = new HttpClientHandler();
            clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) =>
            {
                return true;
            };

            var client = new FlurlClient(new HttpClient(clientHandler));
            foreach (var baseUrl in _config.Urls)
            {
                try
                {
                    var response = await client.Request(baseUrl).AllowAnyHttpStatus().ConfigureRequest(x =>
                    {
                        x.Timeout = TimeSpan.FromSeconds(15);
                    }).PostJsonAsync(filter);
                    var responseContent = await response.Content.ReadAsStringAsync();
                    var tripCollection = JsonConvert.DeserializeObject<List<Trip>>(responseContent);

                    searchResult.Add(tripCollection);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"Error trying to get response from {baseUrl}");
                }
            }
            // Запись в кэш при получении какого-либо ответа от партнерских сервисов
            // Ключом является сам фильтр, значением - результат работы партнерских сервисов
            if (searchResult.Any())
            {
                _cacheStorage.Set(JsonConvert.SerializeObject(filter), JsonConvert.SerializeObject(searchResult));
            }

            return searchResult;
        }


        private async Task FixupReqeust(RequestFilter request)
        {
            try
            {
                using (var ctx = new CoreContext(_contextOptions.Options))
                {
                    var tripSearchParams = _mapper.Map<TripSearchParams>(request);

                    await ctx.AddAsync(tripSearchParams);
                    await ctx.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Request was not added to db");
            }
        }
    }
}
