﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreService.Contract.Enums;
using CoreService.Contract.Request;
using CoreService.Contract.Response;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;

namespace CoreServoce.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;


        private readonly IMemoryCache _cacheStorage;

        public WeatherForecastController(ILogger<WeatherForecastController> logger, IMemoryCache cacheStorage)
        {
            _logger = logger;
            _cacheStorage = cacheStorage;
        }

        [HttpGet("cache-test")]
        public async Task<ActionResult<string>> Get()
        {
            using(var entry = _cacheStorage.CreateEntry("foo")){
                entry.SetValue("bar");
            }

            var itme = _cacheStorage.Get<string>("foo");

            return Ok(itme);
        }

        [HttpPost]
        public IActionResult Post([FromQuery] string somemodel)
        {
            try
            {
                throw new Exception("ex");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Error occured at {nameof(Post)}");
                return BadRequest();
            }

            return Ok();
        }
    }
}
