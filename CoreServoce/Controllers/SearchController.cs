﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CoreService.Business;
using BM = CoreService.Business.Models;
using CoreService.Contract;
using CoreService.Contract.Response;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CoreServoce.Controllers
{
    public class SearchController : Controller
    {

        private readonly IMapper _mapper;
        private readonly ILogger<SearchController> _logger;
        private readonly ISearchService _searchService;

        public SearchController(ISearchService searchService, ILogger<SearchController> logger, IMapper mapper)
        {
            _searchService = searchService;
            _logger = logger;
            _mapper = mapper;
        }

        /// <summary>
        /// Search for trips
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("searchTrips")]
        public async Task<ActionResult> SearchTripsAsync([FromBody] RequestFilter request)
        {
            var response = new List<List<Trip>>();
            try
            {
                var businessRequest = _mapper.Map<BM.Request.RequestFilter>(request);
                var result = await _searchService.SearchTripsAsync(businessRequest);
                response = _mapper.Map<List<List<Trip>>>(result);

                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Exception occured at {nameof(SearchController)} during request execution");
            }

            return Json(response);
        }
    }
}