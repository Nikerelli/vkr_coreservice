using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CoreService.Business;
using CoreService.DataAccess;
using CoreServoce.Configs;
using CoreServoce.Mapping;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using StackExchange.Redis;

namespace CoreServoce
{
    /// <summary/>
    public class Startup
    {
        /// <summary/>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary/>
        public IConfiguration Configuration { get; }

        /// <summary/>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddMvc();

            var config = new SearchServiceConfig();
            config.Urls = Configuration.GetSection("searchConfig").Get<List<string>>();
            services.AddSingleton<ISearchConfig>(config);

            services.AddSingleton<Profile, ContractToBusinessMap>();
            services.SetUpDataAccess();

            services.AddSingleton(provider => 
                new MapperConfiguration(opt => opt.AddProfiles(provider.GetServices<Profile>()))
                    .CreateMapper(provider.GetServices));

            services.AddSwaggerGen(opts =>
            {
                opts.SwaggerDoc("v1", new OpenApiInfo { Title = "My API", Version = "v1" });
            });

            services.AddDbContext<CoreContext>(opts =>
                opts.UseSqlServer(Configuration.GetConnectionString("DefaultCon")));


            services.AddSingleton<DbContextOptionsBuilder<CoreContext>>(new DbContextOptionsBuilder<CoreContext>().UseSqlServer(Configuration.GetConnectionString("DefaultCon")));

            services.AddMemoryCache();
            services.RegisterSearchService();
        }

        /// <summary/>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
                });
            }



            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
