﻿using AutoMapper;
using CoreService.Contract;
using CoreService.Contract.Response;
using BM = CoreService.Business.Models;
using RoutePoint = CoreService.Contract.Request.RoutePoint;

namespace CoreServoce.Mapping
{
    public class ContractToBusinessMap : Profile
    {
        public ContractToBusinessMap()
        {
            CreateMap<Trip, BM.Response.Trip>().ReverseMap();
            CreateMap<RoutePoint, BM.Request.RoutePoint>().ReverseMap();
            CreateMap<RequestFilter, BM.Request.RequestFilter>().ReverseMap();
        }
    }
}
